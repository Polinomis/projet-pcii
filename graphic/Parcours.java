package graphic;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

/**
 * gère le parcours de l'oval
 */

public class Parcours extends Thread{

	public static final Random RAND = new Random();

	private int position;
	private final int POS_INC = 3; // Increment de position
	private final float MAX_PENTE;
	private final int DECALAGE;

	private final int HEIGHT;
	private final int WIDTH;

	private ArrayList<Point> ligneBrisee;

	/** Point servant a determinant la position de l'oval
	 * par rapport a la courbe
	 */

	private Point pa; // Point avant l'oval
	private Point pb; // Point après l'oval

	/**Constructeur
	 *
	 * */

	public Parcours(int xStart, int yStart,int height, int width, int gravite){
		DECALAGE = xStart;
		position = 0;

		HEIGHT = height;
		WIDTH = width;

		MAX_PENTE = (gravite/POS_INC);

		int x = xStart;
		int y = yStart;

		int xsuiv; 
		int ysuiv;
		
		ligneBrisee = new ArrayList<Point>();
		ligneBrisee.add(new Point(x,y));

		while(x < width){

			// On verifie si le nouveau point n'est pas trop bas pour la gravite
			do{
				xsuiv = x + RAND.nextInt(100)+100;
				ysuiv = RAND.nextInt(height);

			}while((float)(ysuiv-y)/(xsuiv-x) > MAX_PENTE);


			x = xsuiv;
			y = ysuiv;

			ligneBrisee.add(new Point(x,y));
		}

		pa = ligneBrisee.get(0);
		pb = ligneBrisee.get(1);
	}

	public ArrayList<Point> getParcoursPoints(){
		return ligneBrisee;
	}

	public int getPosition(){
		return position;
	}

	/**
	 * augmente la position en faisant reculer les points
	 * enleve les points inutiles
	 */
	public void avancer(){
		position += POS_INC;
		int i = 0;
		
		// reculer les points
		for(; i<ligneBrisee.size(); i++){
			ligneBrisee.get(i).translate(-POS_INC,0);
		}

		/**
		 * Modifier les pointeurs de points servant a verifier si l'oval est 
		 * sur la ligne
		 */

		if(ligneBrisee.get(1).x <= DECALAGE){
			pa = ligneBrisee.get(1);
			pb = ligneBrisee.get(2);
		}

		// supprimer le premier une fois que le deuxième à passer la limite
		if(ligneBrisee.get(1).x < 0){
			ligneBrisee.remove(0);
			i--;
		}

		// ajouter  un nouveau point une fois que le dernier arrive a la bordure
		if(ligneBrisee.get(i-1).x <= WIDTH){
			int x = ligneBrisee.get(i-1).x;
			int y = ligneBrisee.get(i-1).y;

			int xsuiv; 
			int ysuiv;

			// On verifie si le nouveau point n'est pas trop bas pour la gravite
			do{
				xsuiv = x + RAND.nextInt(100)+100;
				ysuiv = RAND.nextInt(HEIGHT);

			}while((float)(ysuiv-y)/(xsuiv-x) > MAX_PENTE);

			x = xsuiv;
			y = ysuiv;

			ligneBrisee.add(new Point(x,y));
		}
	}

	/** Getter de pa */
	public Point getPa(){
		return pa;
	}

	/** Getter de pb */
	public Point getPb(){
		return pb;
	}
}
