package graphic;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

/**
 * classe s'occupant des évenements Souris
 *
 */

public class Control implements MouseListener{
	private final Etat ETAT;
	private final Affichage AFFICHAGE;


	public Control(Etat e, Affichage a){
		ETAT = e;
		AFFICHAGE = a;
	}

	//* Fait sauter l'ovale au moment du click sauf si la partie est perdu */
	@Override
	public void mouseClicked(MouseEvent e){
		if(!ETAT.testPerdu())
		{
			ETAT.jump(0);
			AFFICHAGE.repaint();
		}
	};

	@Override
	public void mouseEntered(MouseEvent e){
	};

	@Override
	public void mouseExited(MouseEvent e){};

	@Override
	public void mousePressed(MouseEvent e){};

	@Override
	public void mouseReleased(MouseEvent e){};
}
