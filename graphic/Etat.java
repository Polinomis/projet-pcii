package graphic;

import java.awt.Point;
import java.util.ArrayList;

/**
 * classe s'occupant de l'evolution de l'etat du jeu:
 * faire avancer le parcours
 * faire tomber l'ovale
 * verifier si l'olale est sur le parcours
 *
 */

public class Etat{
	final private Affichage AFFICHAGE;
	final private Control CONTROL;

	public Voler voler;
	public Avancer avancer;
	public Parcours parcours;

	private int hauteur = 200;
	private int ovalX = 100;
	public static final int OVAL_LARG = 50;
	public static final int OVAL_HAUT = 100;

	final private int MAX_HAUTEUR;
	final private int MIN_HAUTEUR;

	final private int SAUT = -20;
	final private int GRAVITE = 3;

	private boolean jeuActif = true;

	/** Constructeur */
	public Etat(Affichage a, int minHauteur, int maxHauteur){
		AFFICHAGE = a;
		CONTROL = new Control(this,AFFICHAGE);
		MIN_HAUTEUR = minHauteur;
		MAX_HAUTEUR = maxHauteur;

		AFFICHAGE.addMouseListener(CONTROL);

		voler = new Voler(this);
		parcours = new Parcours(ovalX,hauteur,maxHauteur,AFFICHAGE.LARGEUR,GRAVITE);
		avancer = new Avancer(parcours,this);

		//Lancement des thread
		voler.start();
		avancer.start();
	}

	/** monte l'oval */
	public void jump(int limite){
		if(hauteur+SAUT >= MIN_HAUTEUR) 
			hauteur+=SAUT;
		else
			hauteur = MIN_HAUTEUR;
	}

	/** descent l'oval */
	public void moveDown(){
		if(hauteur + GRAVITE <= MAX_HAUTEUR)
			hauteur += GRAVITE;
		else
			hauteur = MAX_HAUTEUR;

		AFFICHAGE.repaint();
		AFFICHAGE.revalidate();
	}

	/** fonction de test pour verifier si on a perdu */
	public boolean testPerdu(){
		Point pa = parcours.getPa();
		Point pb = parcours.getPb();


		// Si l'ovale est au dessus de la courbe
		if(hauteur - OVAL_HAUT/2 > (pa.y + ( ovalX - pa.x ) * ( pb.y - pa.y) / (pb.x - pa.x)))
			return true;

		// Si l'ovale est en dessous de la courbe
		if(hauteur + OVAL_HAUT/2 < (pa.y + ( ovalX - pa.x ) * ( pb.y - pa.y) / (pb.x - pa.x)) )
			return true;

		return false;

	}

	//* change la variable jeuActif et affiche la popup */
	public void finirJeu(){
		jeuActif = false;
		AFFICHAGE.afficherPerdu();
	}

	/** getter de hauteur */
	public int getHauteur(){
		return hauteur;
	}

	/** getter de OvalX */
	public int getOvalX(){
		return ovalX;
	}

	/** getter de OVAL_LARG */
	public int getOvalLarg(){
		return OVAL_LARG;
	}

	/** getter de OVAL_HAUT */
	public int getOvalHaut(){
		return OVAL_HAUT;
	}

	public Parcours getParcours(){
		return parcours;
	}

	public Avancer getAvancer(){
		return avancer;
	}

	public Voler getVoler(){
		return voler;
	}

	public boolean getJeuActif(){
		return jeuActif;
	}
}
