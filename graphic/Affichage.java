package graphic;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;

import java.awt.Point;
import java.util.ArrayList;

import java.awt.Color;

import javax.swing.JOptionPane;

/**
 * Gère la zone d'affichage
 *
 */

public class Affichage extends JPanel{
	public static final int LARGEUR = 800;
	public static final int HAUTEUR = 400;

	public Etat etat;

	public Affichage(){
		etat = new Etat(this,0,HAUTEUR);

		setPreferredSize(new Dimension(LARGEUR,HAUTEUR));
	}

	/**
	 * nettoie l'affichage
	 * affiche l'oval ,le parcours et la position
	 */
	@Override
	public void paint(Graphics g){
		g.clearRect(0,0,LARGEUR,HAUTEUR);
		super.paint(g);

		// Affichage de l'oval

		g.drawOval(etat.getOvalX()-etat.getOvalLarg()/2,
				etat.getHauteur()-etat.getOvalHaut()/2,
				etat.getOvalLarg(),
				etat.getOvalHaut());

		// Affichage du parcours

		ArrayList<Point> p = etat.getParcours().getParcoursPoints();

		g.setColor(Color.RED);
		for(int i=0; i<p.size()-1; i++)
		{
			g.drawLine(p.get(i).x,p.get(i).y,p.get(i+1).x,p.get(i+1).y);
		}
		
		// Affichage de la position
		
		g.setColor(Color.BLACK);
		g.drawString(Integer.toString(etat.getParcours().getPosition()),10,10);
	}

	public void afficherPerdu(){
		JOptionPane.showMessageDialog(this,"Perdu!\nScore : "+etat.getParcours().getPosition());
	}
}
