package graphic;

import java.util.Random;

public class Avancer extends Thread{
	final Parcours PARCOURS;
	final Etat ETAT;

	public static final Random RAND = new Random();
	
	/** Constructeur */
	public Avancer(Parcours p,Etat e){
		PARCOURS = p;
		ETAT = e;
	}

	/** appel moveDown dans un autre thread */
	@Override
	public void run(){
		while(ETAT.getJeuActif())
		{
			PARCOURS.avancer();
			if(ETAT.testPerdu())
				ETAT.finirJeu();

			try { 
				Thread.sleep(RAND.nextInt(50)+50); 
			}
			catch (Exception e) { 
				e.printStackTrace(); 
			}
		}
	}
}
