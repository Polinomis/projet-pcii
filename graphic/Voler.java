package graphic;

import java.util.Random;

public class Voler extends Thread{
	final Etat ETAT;

	public static final Random RAND = new Random();
	
	/** Constructeur */
	public Voler(Etat e){
		ETAT = e;
	}

	/** appel moveDown  */
	@Override
	public void run(){
		while(ETAT.getJeuActif())
		{
			ETAT.moveDown();

			try { 
				Thread.sleep(RAND.nextInt(50)+50); 
			}
			catch (Exception e) { 
				e.printStackTrace(); 
			}
		}

	}
}
